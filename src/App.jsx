
import ItemList from './components/ItemList';
import HomePage from './components/HomePage';

function App() {
 
  return (
    <>
    <div className="container mx-auto px-4">
     <HomePage />
     <ItemList  />
     </div>
    </>
  )
}

export default App
