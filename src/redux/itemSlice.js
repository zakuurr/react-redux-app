import { createSlice } from '@reduxjs/toolkit';

const itemSlice = createSlice({
  name: 'items',
  initialState: {
    data:[]
  },
  reducers: {
    addItem: (state, action) => {
      const newItem = { ...action.payload, id: state.length + 1 };
      state.data.push(newItem);
    },
  },
});

export const { addItem } = itemSlice.actions;
export const selectItems = (state) => state.items.data;
export default itemSlice.reducer;
