// src/app/store.js
import { configureStore } from '@reduxjs/toolkit';
import itemReducer from './itemSlice';

export const store = configureStore({
  reducer: {
    items: itemReducer,
  },
});

store.subscribe(() => {
    console.log("store change :", store.getState());
});
