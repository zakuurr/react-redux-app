import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addItem } from '../redux/itemSlice';
const AddItemForm = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const handleAddItem = () => {
    if (!name || !price) {
      alert('Please enter both item name and price.');
      return;
    }
    dispatch(addItem({ name: name, price: price }));
    setName('');
    setPrice('');
  };

  return (
    <div className='mt-3'>
      <h1 className="mb-2 text-2xl font-bold text-blue-600">Tambah Item</h1>
      <form class="w-full max-w-sm">
        <label>
          Name:
          <input className='appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500' type="text" value={name} onChange={(e) => setName(e.target.value)} />
        </label>
        <br />
        <label>
          Price:
          <input className='appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500' type="number" value={price} onChange={(e) => setPrice(e.target.value)} />
        </label>
        <br />
        <button type="button" onClick={handleAddItem} className='bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>
          Add Item
        </button>
      </form>
    </div>
  );
};
export default AddItemForm;
