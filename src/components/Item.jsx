import React from 'react'

export default function Item({item}) {
  return (
    <>
 <div class="flex flex-wrap gap-4 p-2 justify-start text-lg font-serif w-64">
    <a href="#"
        class="bg-gray-100 flex-grow text-black border-l-8 border-blue-500 rounded-md px-3 py-2 w-full md:w-5/12 lg:w-3/12">
        {item.name}
        <div class="text-gray-500 font-thin text-sm pt-1">
            <span>Harga :</span>
            <span>{item.price}</span>
        </div>
    </a>
</div>
    </>
  );
}
