import React from 'react'
import { useSelector } from 'react-redux';
import { selectItems } from '../redux/itemSlice';
function StatItem() {
    const items = useSelector(selectItems);
    const itemCount = items.length;
  return (
    <div>
         <div className='text-center mt-4'>
        <h1 className="mb-2 text-2xl font-bold text-blue-600">Statistik Item</h1>
        <p>Total Items: {itemCount}</p>
        </div>
    </div>
  )
}

export default StatItem