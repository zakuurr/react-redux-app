import React from 'react'
import FeaturedItem from './FeaturedItem'
import StatItem from './StatItem'

function HomePage() {
  return (
    <>
   <div class="justify-start text-lg font-serif">
    <FeaturedItem />
    <StatItem />
  </div>
    </>
  )
}
export default HomePage