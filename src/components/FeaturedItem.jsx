import React from 'react'
import { useSelector } from 'react-redux';
import { selectItems } from '../redux/itemSlice';
function FeaturedItem() {
  const items = useSelector(selectItems);
  const featuredItem = items.reduce((min, current) => {
        return current.price < min.price ? current : min;
      }, items[0]);
  return (
    <>
    <div className='text-center'>
        <h1 className="mb-2 text-2xl font-bold text-blue-600">Featured Item</h1>
        {featuredItem && (
       <>
       <p>Nama Item: {featuredItem.name}</p>
        <p>Harga Item: {featuredItem.price}</p>
        </>      
      )}
</div>
    </>
  )
}
export default FeaturedItem