import React, { useState } from 'react';
import Item from './Item';
import AddItemForm from './AddItemForm';
import { useSelector } from 'react-redux';
import { selectItems } from '../redux/itemSlice';
function ItemList() {
  const items = useSelector(selectItems);

  return (
   <>
   <div class="flex justify-start text-lg font-serif">
   <AddItemForm />
     </div>
     <h1 className="mb-2 text-2xl font-bold text-blue-600 mt-4">List Item</h1>
   <div class="justify-center text-lg font-serif mt-3">
      {items.map(item => (
        <Item key={item.id} item={item} />
      ))}
    </div>
   </>
  )
}
export default ItemList